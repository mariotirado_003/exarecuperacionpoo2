package controlador;

/**
 *
 * @author luism
 */
import modelo.Recibo;
import vista.dlgRecibo;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class ExamenrecuperacionPOO2 implements ActionListener{

     private Recibo rec;
     private dlgRecibo vista;
     
      public ExamenrecuperacionPOO2(Recibo rec, dlgRecibo vista) {
        this.rec = rec;
        this.vista = vista;
        
        
        // se inicializan los botonos
        vista.btnGuardar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
      }
      
      
       private void iniciarvista() {
        vista.setTitle("Pagos");
        vista.setSize(596, 528);
        vista.setVisible(true);
    }
       
       
    
    
    @Override
       public void actionPerformed(ActionEvent e) {
        
           
                   //btn nuevo
        if (e.getSource() == vista.btnNuevo) {
            vista.txtnumRecibo.setEnabled(true);
            vista.txtFecha.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtDomicilio.setEnabled(true);
            vista.cmbtipodeServicio.setEnabled(true);
            vista.txtcostoporKw.setEnabled(true);
            vista.txtkwConsumidos.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            
            
        }
        //btn cerrar
        if (e.getSource() == vista.btnCerrar) {
            int respuesta = JOptionPane.showConfirmDialog(vista, "¿Quieres salir",
                    "Salir", JOptionPane.YES_NO_OPTION);
            if (respuesta == JOptionPane.YES_NO_OPTION) {
                vista.dispose();
                System.exit(0);
            }
        }
        if (e.getSource() == vista.btnLimpiar) {
            vista.txtnumRecibo.setText("");
            vista.txtFecha.setText("");
            vista.txtNombre.setText("");
            vista.txtDomicilio.setText("");
            
            //vista.cmbtipodeServicio.setText("");
            vista.txtcostoporKw.setText("");
            vista.txtkwConsumidos.setText("");
            vista.txtSubtotal.setText("");
            vista.txtImpuesto.setText("");
            vista.txtTotalaPagar.setText("");

        }
        if (e.getSource() == vista.btnCancelar) {
            vista.txtnumRecibo.setText("");
            vista.txtFecha.setText("");
            vista.txtNombre.setText("");
            vista.txtDomicilio.setText("");
            
            //vista.cmbtipodeServicio.setText("");
            vista.txtcostoporKw.setText("");
            vista.txtkwConsumidos.setText("");
            vista.txtSubtotal.setText("");
            vista.txtImpuesto.setText("");
            
            vista.txtnumRecibo.setEnabled(false);
            vista.btnGuardar.setEnabled(false);
            vista.cmbtipodeServicio.setEnabled(false);
            //vista.jComboBox1.setEnabled(false);
            vista.txtNombre.setEnabled(false);
            vista.btnCancelar.setEnabled(false);
            vista.txtDomicilio.setEnabled(false);
            vista.txtFecha.setEnabled(false);
            vista.btnLimpiar.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            vista.txtcostoporKw.setEnabled(false);
            
            vista.txtkwConsumidos.setEnabled(false);
            vista.txtSubtotal.setEnabled(false);
            vista.txtImpuesto.setEnabled(false);
        }
        if (e.getSource() == vista.btnGuardar) {
            try{
               
            rec.setNumRecibo(Integer.parseInt(vista.txtnumRecibo.getText()));
            rec.setFecha(vista.txtFecha.getText());
            rec.setNombre(vista.txtNombre.getText());
            rec.setDomicilio(vista.txtDomicilio.getText());
            rec.setTipoServ(vista.cmbtipodeServicio.getSelectedIndex());
            rec.setCosto(Float.parseFloat(vista.txtcostoporKw.getText()));
            rec.setKwConsumidos(Float.parseFloat(vista.txtkwConsumidos.getText()));
            
            JOptionPane.showMessageDialog(vista, "Guardado con exito");
            
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error" + ex.getMessage());
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error" + ex2.getMessage());
            }
            
            //doc.setNivel(Integer.parseInt(vista.spicantHijos.getValue().toString()));

            
        }
        
            if (e.getSource() == vista.btnMostrar) {
            vista.txtDomicilio.setText(String.valueOf(rec.getDomicilio()));
            vista.txtnumRecibo.setText(String.valueOf(rec.getNumRecibo()));
            vista.txtNombre.setText(String.valueOf(rec.getNombre()));
            vista.txtFecha.setText(String.valueOf(rec.getFecha()));
            vista.txtcostoporKw.setText(String.valueOf(rec.getCosto()));
            vista.txtImpuesto.setText(String.valueOf(rec.calcularImpuesto()));
            vista.txtkwConsumidos.setText(String.valueOf(rec.getKwConsumidos()));
            vista.txtSubtotal.setText(String.valueOf(rec.calcularSubtotal()));
            vista.txtTotalaPagar.setText(String.valueOf(rec.calcularTotal()));
            vista.cmbtipodeServicio.getSelectedIndex();
            
        }
    }

    

       public static void main(String[] args) {
        Recibo rec = new Recibo();
        dlgRecibo vista = new dlgRecibo(new JFrame(), true);
        ExamenrecuperacionPOO2 contra = new ExamenrecuperacionPOO2(rec, vista);
        contra.iniciarvista();
    }
}

