package modelo;

public class Recibo {
    private int numRecibo,tipoServ;
    private String nombre,domicilio,fecha;
    private float kwConsumidos,costo;

    public Recibo(int numRecibo, int tipoServ, String nombre, String domicilio, String fecha, float kwConsumidos, float costo) {
        this.numRecibo = numRecibo;
        this.tipoServ = tipoServ;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.fecha = fecha;
        this.kwConsumidos = kwConsumidos;
        this.costo = costo;
    }
        public Recibo(){
        this.numRecibo = 0;
        this.nombre = "";
        this.domicilio = "";
        this.fecha = "";
        this.tipoServ = 0;
        this.costo = 0;
        this.kwConsumidos = 0;
    }
    
    public Recibo(Recibo obj) {
        this.numRecibo = obj.numRecibo;
        this.tipoServ = obj.tipoServ;
        this.nombre = obj.nombre;
        this.domicilio = obj.domicilio;
        this.fecha = obj.fecha;
        this.kwConsumidos = obj.kwConsumidos;
        this.costo = obj.costo;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public int getTipoServ() {
        return tipoServ;
    }

    public void setTipoServ(int tipoServ) {
        this.tipoServ = tipoServ;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public float getKwConsumidos() {
        return kwConsumidos;
    }

    public void setKwConsumidos(float kwConsumidos) {
        this.kwConsumidos = kwConsumidos;
    }

    public float getCosto() {
        return costo;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }
    

    
    
    
    public float calcularSubtotal(){
        float auxtotal=0.0f;
        switch (tipoServ) {
            case 1:
                costo = 2;
                break;
            case 2:
                costo = 3;
                break;
            case 3:
                costo = 5;
                break;
        }
        return costo * kwConsumidos;
    }
    
    public double calcularImpuesto() {
        double pagoTotal = calcularSubtotal();
        return pagoTotal * 0.16;
    }
        
    public double calcularTotal(){
        return calcularSubtotal() + calcularImpuesto();
    }
    
}
